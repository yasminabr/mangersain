﻿using MangerSain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MangerSain.DAL
{
    public class MangerSainContext : DbContext
    {
        public DbSet<Administrateur> Administrateurs { get; set; }
        public DbSet<Boissons> Boissons { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Commande> Commandes { get; set; }
        public DbSet<Items> Items { get; set; }
        public DbSet<Jus> Jus { get; set; }
        public DbSet <Lait>Laits { get; set; }
        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<TheTisane> TheTisanes { get; set; }
        public DbSet<Panier> Paniers { get; set; }  
        public DbSet<Noix> Noix { get; set; }
        public DbSet<Legume> Legumes { get; set; }
        public DbSet<PanierItem> PanierItems { get; set; }

    }
}