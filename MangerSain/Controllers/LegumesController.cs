﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MangerSain.DAL;
using MangerSain.Models;

namespace MangerSain.Controllers
{
    public class LegumesController : Controller
    {
        private MangerSainContext db = new MangerSainContext();

        // GET: Legumes
        public ActionResult Index()
        {
            return View(db.Legumes.ToList());
        }

        // GET: Legumes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Legume legume = db.Legumes.Find(id);
            if (legume == null)
            {
                return HttpNotFound();
            }

            ViewBag.Clients = db.Clients.ToList();

            return View(legume);
        }

        // GET: Legumes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Legumes/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom,Prix,Description,Provenance")] Legume legume)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(legume);
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }

            return View(legume);
        }

        // GET: Legumes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Legume legume = db.Legumes.Find(id);
            if (legume == null)
            {
                return HttpNotFound();
            }
            return View(legume);
        }

        // POST: Legumes/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom,Prix,Description,Provenance")] Legume legume)
        {
            if (ModelState.IsValid)
            {
                db.Entry(legume).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }
            return View(legume);
        }

        // GET: Legumes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Legume legume = db.Legumes.Find(id);
            if (legume == null)
            {
                return HttpNotFound();
            }
            return View(legume);
        }

        // POST: Legumes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Legume legume = db.Legumes.Find(id);
            db.Items.Remove(legume);
            db.SaveChanges();
            return RedirectToAction("Index", "Items");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
