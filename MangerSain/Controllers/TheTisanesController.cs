﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MangerSain.DAL;
using MangerSain.Models;

namespace MangerSain.Controllers
{
    public class TheTisanesController : Controller
    {
        private MangerSainContext db = new MangerSainContext();

        // GET: TheTisanes
        public ActionResult Index()
        {
            return View(db.Items.ToList());
        }

        // GET: TheTisanes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TheTisane theTisane = db.TheTisanes.Find(id);
            if (theTisane == null)
            {
                return HttpNotFound();
            }

            ViewBag.Clients = db.Clients.ToList();

            return View(theTisane);
        }

        // GET: TheTisanes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TheTisanes/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom,Prix,Description,Cafeine")] TheTisane theTisane)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(theTisane);
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }

            return View(theTisane);
        }

        // GET: TheTisanes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TheTisane theTisane = db.TheTisanes.Find(id);
            if (theTisane == null)
            {
                return HttpNotFound();
            }
            return View(theTisane);
        }

        // POST: TheTisanes/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom,Prix,Description,Cafeine")] TheTisane theTisane)
        {
            if (ModelState.IsValid)
            {
                db.Entry(theTisane).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }
            return View(theTisane);
        }

        // GET: TheTisanes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TheTisane theTisane = db.TheTisanes.Find(id);
            if (theTisane == null)
            {
                return HttpNotFound();
            }
            return View(theTisane);
        }

        // POST: TheTisanes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TheTisane theTisane = db.TheTisanes.Find(id);
            db.Items.Remove(theTisane);
            db.SaveChanges();
            return RedirectToAction("Index", "Items");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
