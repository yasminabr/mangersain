﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MangerSain.DAL;
using MangerSain.Models;

namespace MangerSain.Controllers
{
    public class JusController : Controller
    {
        private MangerSainContext db = new MangerSainContext();

        // GET: Jus
        public ActionResult Index()
        {
            return View(db.Jus.ToList());
        }

        // GET: Jus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jus jus = db.Jus.Find(id);
            if (jus == null)
            {
                return HttpNotFound();
            }
            ViewBag.Clients = db.Clients.ToList();

            return View(jus);
        }

        // GET: Jus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Jus/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom,Prix,Description,Concentration,Volume")] Jus jus)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(jus);
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }

            return View(jus);
        }

        // GET: Jus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jus jus = db.Jus.Find(id);
            if (jus == null)
            {
                return HttpNotFound();
            }
            return View(jus);
        }

        // POST: Jus/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom,Prix,Description,Concentration")] Jus jus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(jus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Items");
            }
            return View(jus);
        }

        // GET: Jus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jus jus = db.Jus.Find(id);
            
            if (jus == null)
            {
                return HttpNotFound();
            }
            return View(jus);
        }

        // POST: Jus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Jus jus = db.Jus.Find(id);
            db.Items.Remove(jus);
            db.SaveChanges();
            return RedirectToAction("Index", "Items");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
