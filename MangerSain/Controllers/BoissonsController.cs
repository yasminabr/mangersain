﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MangerSain.DAL;
using MangerSain.Models;

namespace MangerSain.Controllers
{
    public class BoissonsController : Controller
    {
        private MangerSainContext db = new MangerSainContext();

        // GET: Boissons
        public ActionResult Index()
        {
            return View(db.Items.ToList());
        }

        // GET: Boissons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boissons boissons = db.Boissons.Find(id);
            if (boissons == null)
            {
                return HttpNotFound();
            }
            return View(boissons);
        }

        // GET: Boissons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Boissons/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom,Prix,Description,Volume")] Boissons boissons)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(boissons);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(boissons);
        }

        // GET: Boissons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boissons boissons = db.Boissons.Find(id);
            if (boissons == null)
            {
                return HttpNotFound();
            }
            return View(boissons);
        }

        // POST: Boissons/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom,Prix,Description")] Boissons boissons)
        {
            if (ModelState.IsValid)
            {
                db.Entry(boissons).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(boissons);
        }

        // GET: Boissons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boissons boissons = db.Boissons.Find(id);
            if (boissons == null)
            {
                return HttpNotFound();
            }
            return View(boissons);
        }

        // POST: Boissons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Boissons boissons = db.Boissons.Find(id);
            db.Items.Remove(boissons);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
