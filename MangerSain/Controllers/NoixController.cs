﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MangerSain.DAL;
using MangerSain.Models;

namespace MangerSain.Controllers
{
    public class NoixController : Controller
    {
        private MangerSainContext db = new MangerSainContext();

        // GET: Noixes
        public ActionResult Index()
        {
            return View(db.Items.ToList());
        }

        // GET: Noixes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Noix noix = db.Noix.Find(id);
            if (noix == null)
            {
                return HttpNotFound();
            }

            ViewBag.Clients = db.Clients.ToList();

            return View(noix);
        }

        // GET: Noixes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Noixes/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom,Prix,Description,Etat,Coquille")] Noix noix)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(noix);
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }

            return View(noix);
        }

        // GET: Noixes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Noix noix = db.Noix.Find(id);
            if (noix == null)
            {
                return HttpNotFound();
            }
            return View(noix);
        }

        // POST: Noixes/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom,Prix,Description,Etat,Coquille")] Noix noix)
        {
            if (ModelState.IsValid)
            {
                db.Entry(noix).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }
            return View(noix);
        }

        // GET: Noixes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Noix noix = db.Noix.Find(id);
            if (noix == null)
            {
                return HttpNotFound();
            }
            return View(noix);
        }

        // POST: Noixes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Noix noix = db.Noix.Find(id);
            db.Items.Remove(noix);
            db.SaveChanges();
            return RedirectToAction("Index", "Items");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
