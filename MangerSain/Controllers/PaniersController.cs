﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MangerSain.DAL;
using MangerSain.Models;

namespace MangerSain.Controllers
{
    public class PaniersController : Controller
    {
        public string NomClient;
        private MangerSainContext db = new MangerSainContext();

        // GET: Paniers
        public ActionResult Index([Bind(Include = "Nom,Prix")] Items items)
        {
            return View(db.Paniers.ToList());
        }

        // GET: Paniers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Panier panier = db.Paniers.Find(id);
            if (panier == null)
            {
                return HttpNotFound();
            }
            ViewBag.Clients = db.Clients.ToList();
            return View(panier);
        }

        [HttpPost]
        public ActionResult AjouterItem(int id)
        {
            Items item = db.Items.Find(id);

            if (item == null)
            {
                return HttpNotFound();
            }

            string ListeClient = Request.Form.Get("ListClient");
            if (String.IsNullOrEmpty(ListeClient))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(int.Parse(ListeClient));
            if (client.PanierClient == null)
            {
                client.PanierClient = new Panier
                {
                    ListeItems = new List<PanierItem>()
                };
                db.Paniers.Add(client.PanierClient);
                db.SaveChanges();
            }
            PanierItem panierItem = client.PanierClient.ListeItems.Find(Pi => Pi.Article.Id == id);

            if (panierItem == null)
            {
                panierItem = new PanierItem
                {
                    Article = item,
                    Quantite = 1
                };
                client.PanierClient.ListeItems.Add(panierItem);
            }
            else
            {
                client.PanierClient.ListeItems.Find(Pi => Pi.Article.Id == id).Quantite++;
            }
            client.PanierClient.Total = 0;
            foreach (PanierItem element in client.PanierClient.ListeItems)
            {
                client.PanierClient.Total += element.Article.Prix * element.Quantite;
            }



            db.Entry(client.PanierClient).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Details", new { Id = client.PanierClient.Id });
        }

        public ActionResult AjouterItemPlus(int idItem, int idPanier, int quantiteModif)
        {


            Panier panier = db.Paniers.Find(idPanier);
           
            

            PanierItem panierItem =panier.ListeItems.Find(Pi => Pi.Id == idItem);

            if (panierItem == null)
            {

                return HttpNotFound();
            }
            if (panierItem.Quantite + quantiteModif <= 0)
            {
                return RedirectToAction("Details", new { Id = idPanier });
            }
            panierItem.Quantite += quantiteModif;
            panier.Total = 0;
            foreach (PanierItem element in panier.ListeItems)
            {
                panier.Total += element.Article.Prix * element.Quantite;
            }

            db.Entry(panier).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Details", new { Id = idPanier });
        }


        // GET: Paniers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Paniers/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ListeItems,Total")] Panier panier)
        {
            if (ModelState.IsValid)
            {
                db.Paniers.Add(panier);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(panier);
        }

        // GET: Paniers/Edit/5
        public ActionResult Edit(int id)
        {
            string ListeClient = Request.Form.Get("ListClient");
            if (String.IsNullOrEmpty(ListeClient))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(int.Parse(ListeClient));
            //if (id == null)
            //{
            //  return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            foreach (PanierItem element in client.PanierClient.ListeItems)
            {
                element.Quantite++;
            }
            //Panier panier = db.Paniers.Find(id);
            // if (panier == null)
            // {
            //  return HttpNotFound();
            //}
            return View();
        }

        // POST: Paniers/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Total")] Panier panier)
        {
            if (ModelState.IsValid)
            {
                db.Entry(panier).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(panier);
        }



        // GET: Paniers/Delete/5
        public ActionResult Delete(int idItem, int idPanier)
        {

            Panier panier = db.Paniers.Find(idPanier);

            PanierItem panierItem = panier.ListeItems.Find(Pi => Pi.Id == idItem);

            if (panierItem == null)
            {

                return HttpNotFound();
            }
          
            db.PanierItems.Remove(panierItem);
            db.SaveChanges();
            panier.Total = 0;
            foreach (PanierItem element in panier.ListeItems)
            {
                panier.Total += element.Article.Prix * element.Quantite;
            }


            db.Entry(panier).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Details", new { Id = idPanier });
        }


        public ActionResult AjoutQuantite(int? idPanier)
        {

            string a = "1";
            string ListeClient = Request.Form.Get("ListClient");
            Panier panier = db.Paniers.Find(idPanier);
            Client client = db.Clients.Find(int.Parse(a));



            foreach (PanierItem element in client.PanierClient.ListeItems)
            {
                if (1 == element.Id)
                    element.Quantite++;
            }

            db.SaveChanges();
            return View(panier);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
