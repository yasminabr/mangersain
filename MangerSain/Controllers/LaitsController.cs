﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MangerSain.DAL;
using MangerSain.Models;

namespace MangerSain.Controllers
{
    public class LaitsController : Controller
    {
        private MangerSainContext db = new MangerSainContext();

        // GET: Laits
        public ActionResult Index()
        {
            return View(db.Laits.ToList());
        }

        // GET: Laits/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lait lait = db.Laits.Find(id);
            if (lait == null)
            {
                return HttpNotFound();
            }

            ViewBag.Clients = db.Clients.ToList();

            return View(lait);
        }

        // GET: Laits/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Laits/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom,Prix,Description,MatièreGrasse,Volume")] Lait lait)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(lait);
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }

            return View(lait);
        }

        // GET: Laits/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lait lait = db.Laits.Find(id);
            if (lait == null)
            {
                return HttpNotFound();
            }
            return View(lait);
        }

        // POST: Laits/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom,Prix,Description,MatièreGrasse")] Lait lait)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lait).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Items");
            }
            return View(lait);
        }

        // GET: Laits/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lait lait = db.Laits.Find(id);
            if (lait == null)
            {
                return HttpNotFound();
            }
            return View(lait);
        }

        // POST: Laits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Lait lait = db.Laits.Find(id);
            db.Items.Remove(lait);
            db.SaveChanges();
            return RedirectToAction("Index", "Items");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
