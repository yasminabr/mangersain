namespace MangerSain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mangersain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Utilisateurs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false),
                        Prenom = c.String(nullable: false),
                        Login = c.String(nullable: false, maxLength: 20),
                        MotDePasse = c.String(nullable: false),
                        TypeUtilisateur = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        PanierClient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Paniers", t => t.PanierClient_Id)
                .Index(t => t.PanierClient_Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false),
                        Prix = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(maxLength: 200),
                        Concentration = c.Int(),
                        MatièreGrasse = c.String(),
                        Cafeine = c.Int(),
                        Provenance = c.Int(),
                        Etat = c.Int(),
                        Coquille = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Administrateur_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Utilisateurs", t => t.Administrateur_Id)
                .Index(t => t.Administrateur_Id);
            
            CreateTable(
                "dbo.Paniers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DateCommande = c.DateTime(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Client_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Utilisateurs", t => t.Client_Id)
                .Index(t => t.Client_Id);
            
            CreateTable(
                "dbo.PanierItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantite = c.Int(nullable: false),
                        Article_Id = c.Int(),
                        Panier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.Article_Id)
                .ForeignKey("dbo.Paniers", t => t.Panier_Id)
                .Index(t => t.Article_Id)
                .Index(t => t.Panier_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Utilisateurs", "PanierClient_Id", "dbo.Paniers");
            DropForeignKey("dbo.PanierItems", "Panier_Id", "dbo.Paniers");
            DropForeignKey("dbo.Paniers", "Client_Id", "dbo.Utilisateurs");
            DropForeignKey("dbo.PanierItems", "Article_Id", "dbo.Items");
            DropForeignKey("dbo.Items", "Administrateur_Id", "dbo.Utilisateurs");
            DropIndex("dbo.PanierItems", new[] { "Panier_Id" });
            DropIndex("dbo.PanierItems", new[] { "Article_Id" });
            DropIndex("dbo.Paniers", new[] { "Client_Id" });
            DropIndex("dbo.Items", new[] { "Administrateur_Id" });
            DropIndex("dbo.Utilisateurs", new[] { "PanierClient_Id" });
            DropTable("dbo.PanierItems");
            DropTable("dbo.Paniers");
            DropTable("dbo.Items");
            DropTable("dbo.Utilisateurs");
        }
    }
}
