﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace MangerSain.Models
{
    public class Boissons : Items
    {
        [Display(Name = "Volume")]
        [Required(ErrorMessage = "Volume requis.")]
        public ushort Volume { get; set; }
    }

}