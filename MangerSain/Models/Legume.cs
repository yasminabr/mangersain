﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Legume : Items
    {
        [Required(ErrorMessage = "Veuillez spécifier la région")]
        [Display(Name ="Fabriquer au :")]
        public TypeProvenance Provenance { get; set; }
    }
    public enum TypeProvenance
    {
        USA,
        Canada,
        Mexique
    }
}