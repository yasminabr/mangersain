﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Lait : Boissons
    {
        [Display(Name ="% de matière grasse")]
        [Required(ErrorMessage = "Champs requis ! ")]
        public string MatièreGrasse { get; set; }
    }
}