﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Items
    {
        
        public int Id { get; set; }
        [Required(ErrorMessage = "Votre boisson doit avoir un nom.")]
        public string Nom { get; set; }

        [Display(Name ="Prix")]
        [Required(ErrorMessage = "Votre boisson doit avoir un prix.")]
        public decimal Prix { get; set; }

        [MaxLength(200, ErrorMessage ="Pas plus de 200 caractères.")]
        public  string Description { get; set; }



        //test
    }
}