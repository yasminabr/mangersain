﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Administrateur : Utilisateur
    {
        [Display(Name="Historique des items ajouté")]
        public virtual List<Items> ListeItems { get; set; }
    }
}