﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Client : Utilisateur
    {
        [Display(Name="Historique des commandes")]
        public virtual List <Commande> ListeCommades { get; set; }
        [Display(Name ="Ton panier")]
        public virtual Panier PanierClient { get; set; }
    }
}