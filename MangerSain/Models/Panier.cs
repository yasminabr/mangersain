﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Panier
    {
        public int Id { get; set; }

        [Display(Name = "Liste des Éléments")]
        public virtual List<PanierItem> ListeItems { get; set; }

        [DataType(DataType.Currency)]
        public decimal Total { get; set; }
    }
}