﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Noix : Items
    {
        [Required(ErrorMessage = "Choisir le bon état")]
        [Display(Name ="Avec coquille?")]
        public TypeEtat Etat { get; set; }

        [Display(Name ="L'enrobage")]
        [Required(ErrorMessage = "Choisir l'enrobage")]
        public TypeCoquille Coquille { get; set; }
    }

    public enum TypeCoquille
    {
        avec,
        sans
    }
    public enum TypeEtat
    {
        grillé,
        nature,
        salé
    }

}