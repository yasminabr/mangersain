﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Jus : Boissons
    {
        [Required(ErrorMessage = "Champs obligatoire")]
        [Display(Name = "Fait de concentré ?")]
        public Concentration Concentration { get; set; }

    }
    public enum Concentration
    {
        Oui,
        Non
    }
}