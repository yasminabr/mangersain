﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class Utilisateur
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Dois contenir votre nom")]
        public string Nom { get; set; }
        [Required(ErrorMessage = "Dois contenir votre prénom")]
        [Display(Name="Prénom")]
        public string Prenom { get; set; }

        [Display(Name ="Nom d'utilisateur")]
        [MaxLength(20, ErrorMessage ="Pas plus de 20 caractères")]
        [Required(ErrorMessage = "Nom pour vous connecter")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Mot de passe requis")]
        [Display(Name ="Mot de passe")]
        public string MotDePasse { get; set; }
        
        public string TypeUtilisateur { get; set; }
    }
}