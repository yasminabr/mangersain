﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class TheTisane : Boissons
    {
        [Display(Name= "teneur en caféine")]  
        [Required(ErrorMessage = "Champs requis")]
        public TypeCafeine Cafeine { get; set; }
    }
    public enum TypeCafeine
    {
        Avec,
        Sans
    }
}