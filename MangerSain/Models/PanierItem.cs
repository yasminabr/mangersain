﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MangerSain.Models
{
    public class PanierItem
    {
        public int Id { get; set; }
        [Display(Name = "Article")]
        public virtual Items Article { get; set; }

        [Display(Name = "Quantité")]
        public int Quantite { get; set; }
    }
}